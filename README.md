AWS DNS Resolver
================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Creates a route53 DNS Resolver in your VPC.
This resolver can be used to relay DNS requests to your internal hosted zones.

Resources created are:
* ResolverEndpoint
* SecurityGroup

A test playbook has been supplied which rolls out the lambda function included with this role.
You can run this playbook using the following command:
```bash
ansible-playbook aws-dns-resolver/tests/test.yml --inventory aws-dns-resolver/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-setup and aws-vpc role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
* aws-setup
* aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
```
Role Defaults
-------------
```yaml
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"

aws_dns_resolver_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  ip1             : "{{ lookup('cfexport', environment_abbr + '-vpc-SubnetPrivateCIDR1') | ipaddr(-2) | ipaddr('address') }}"
  subnet_nr1      : 0
  ip2             : "{{ lookup('cfexport', environment_abbr + '-vpc-SubnetPrivateCIDR2') | ipaddr(-2) | ipaddr('address') }}"
  subnet_nr2      : 1
```
Example Playbooks
----------------
Deploy resolver  using custom defaults
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
  roles:
    - aws-setup
    - aws-vpc
    - aws-dns-resolver
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>
